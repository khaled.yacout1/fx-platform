class CreateTransactions < ActiveRecord::Migration[7.0]
  def change
    create_table :transactions do |t|
      t.references :customer, null: false, foreign_key: true
      t.monetize :in_amount
      t.monetize :out_amount
      t.datetime :date, null: false

      t.timestamps
    end
  end
end
