class UpdateStateColumnInTransactionsTable < ActiveRecord::Migration[7.0]
  def change
    change_column :transactions, :state, :string, default: 'initial'
  end
end
