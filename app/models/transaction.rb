class Transaction < ApplicationRecord
  include AASM

  enum :state, { initial: 0, pending: 1, completed: 2, failed: 3 }

  aasm column: :state, enum: true do
    state :initial, initial: true
    state :pending
    state :completed
    state :failed

    event :pending do
      transitions from: :initial, to: :pending
    end

    event :completed do
      transitions from: :initial, to: :completed
    end

    event :failed do
      transitions from: :initial, to: :failed
    end
  end

  belongs_to :customer
  validates :date, presence: true
end
