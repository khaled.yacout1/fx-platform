class TransactionsController < ApplicationController
  def index
    transactions = Transaction.all

    render json: {
      data: transactions.as_json(only: %i[id state customer_id in_amount_cents in_amount_currency out_amount_cents out_amount_currency date]),
      status: :ok
    }
  end

  def show
    transaction = Transaction.find_by(id:params[:id])

    if transaction
      render json: {
        data: transaction.as_json(only: %i[id state customer_id in_amount_cents in_amount_currency out_amount_cents out_amount_currency date]),
        status: :ok
      }
    else
      render json: {
        data: [],
        status: :not_found
      }
    end
  end

  def create
    transaction = Transaction.new(transaction_params)

    if transaction.save
      render json: {
        data: transaction.as_json(only: %i[id customer_id in_amount_cents in_amount_currency out_amount_cents out_amount_currency date]),
        status: :created
      }
    else
      render json: {
        data: transaction.errors,
        status: :unprocessable_entity
      }
    end
  end

  def pending_state
    transaction = Transaction.find_by(id:params[:id])

    if transaction
      transaction.pending!
      render json: {
        data: transaction.as_json(only: %i[id state customer_id in_amount_cents in_amount_currency out_amount_cents out_amount_currency date]),
        status: :ok
      }
    else
      render json: {
        data: transaction.errors,
        status: :unprocessable_entity
      }
    end
  end

  def completed_state
    transaction = Transaction.find_by(id:params[:id])

    if transaction
      transaction.completed!
      render json: {
        data: transaction.as_json(only: %i[id state customer_id in_amount_cents in_amount_currency out_amount_cents out_amount_currency date]),
        status: :ok
      }
    else
      render json: {
        data: transaction.errors,
        status: :unprocessable_entity
      }
    end
  end

  def failed_state
    transaction = Transaction.find_by(id:params[:id])

    if transaction
      transaction.failed!
      render json: {
        data: transaction.as_json(only: %i[id state customer_id in_amount_cents in_amount_currency out_amount_cents out_amount_currency date]),
        status: :ok
      }
    else
      render json: {
        data: transaction.errors,
        status: :unprocessable_entity
      }
    end
  end

  def error
    render json: { message: "fx: wrong end point", status: :unprocessable_entity }
  end

  private

  def transaction_params
    params.permit(:customer_id, :in_amount_cents, :in_amount_currency, :out_amount_currency, :out_amount_cents, :date) || nil
  end
end
