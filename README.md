# FX Platform Microservice

> Rails API to serve as a microservice for `fx-platform`

> The API allows you to Do RESTfull actions with each transaction like `list` transactions, `create` and `show` transaction.

## Built With

- Rails 7 ~ _Ruby Web development framework_

## Getting Started

To get a local copy up and running follow these simple example steps

- From your terminal enter in sequence
  - `git clone git@github.com:od-c0d3r/fx-platform.git`.
  - Open created directory `fx-platform` with your desired code editor.
### Available Scripts

In the project directory, run in order:

- #### `rails db:create`

  - Creates the project database.

- #### `rails db:migrate`

  - Creates the tables in the database.

- #### `rails db:seed`

  - Populate the tables with default records.

- #### `rails server`

  - Runs the app in the development mode.\
Open `http://localhost:PORT` to view it in your browser.

### API Documention

#### End points

1. `GET` `/transactions`
    - List all transactions in system.
    - Example
```js
// Request
fetch('http://our-api-host.com/transactions')
  .then((response) => response.json())
  .then((data) => console.log(data));

//Response
{
  data: [
          {
            "id": "1",
            "customer_id": "3",
            "in_currency":	"USD",
            "in_amount": "0", 
            "out_currency":	"GBP",
            "out_amount": "0"
          },
          {
            "id": "2",
            "customer_id": "7",
            "in_currency":	"USD",
            "in_amount": "0",
            "out_currency":	"GBP",
            "out_amount": "0"
          },
          ,...
         ],
  status: 200
}
```

2. `GET` `/transactions/:id`
    - List transaction details with an id of `:id`
    - Example
```js
// Request
fetch('http://our-api-host.com/transactions/${id}')
  .then((response) => response.json())
  .then((data) => console.log(data));

//Response
{
  date: {
      "id": "2",
      "customer_id": "7",
      "in_currency":	"USD",
      "in_amount": "0",
      "out_currency":	"GBP",
      "out_amount": "0"
},
  status: 200
}
```


3. `POST` `/transactions/`
    - Creats a new transaction
    - Request body should have at minimum:
      - `customer_id`   => Customer id which sending or receiving a TXN.
      - `in_amount`     => In amount
      - `in_currency`   => In currency
      - `out_amount`    => Out amount
      - `out_currency`  => Out currency
    - Example
```js
// Request
postData('https://our-api-host.com/transactions', { "customer_id": 42, "txn_amount": 202, "txn_type": 0 })
  .then((data) => {
    console.log(data); // JSON data parsed by `data.json()` call
  });


//Response
{
  date: {
      "id": "2",
      "customer_id": "7",
      "in_currency":	"USD",
      "in_amount": "0",
      "out_currency":	"GBP",
      "out_amount": "0", 
      "date":	"2022-10-25T11:12:06.371Z"
  },
  status: 201
}
```
