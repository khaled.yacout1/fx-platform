Rails.application.routes.draw do
  resources :transactions, only: %i[index show create]

  post '/transactions/:id/pending', to: 'transactions#pending_state'
  post '/transactions/:id/completed', to: 'transactions#completed_state'
  post '/transactions/:id/failed', to: 'transactions#failed_state'

  root 'transactions#error'             , defaults: { format: 'json' }
  get  '*path', to: 'transactions#error', defaults: { format: 'json' }
end
